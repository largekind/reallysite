#!/bin/bash

# ctrl+c時、インスタンス自動終了
trap "final; exit 1" 2

function final {
  echo "Ctrl+C pushed."
  gcloud sql instances patch  really-site-instance --activation-policy=NEVER
}

# インスタンス開始 、 SQLサーバー接続状態に移行
gcloud sql instances patch  really-site-instance --activation-policy=ALWAYS

# SQL接続
echo "If you want to stop instance , please push ctrl+c"
./secrets/cloud_sql_proxy -instances="elegant-gearing-321508:us-central1:really-site-instance"=tcp:3306
