from django import forms
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib.auth.views import LoginView
from django.contrib import messages
from mysite.forms import UserCreationForm , ProfileForm
from blog.models import Article
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login
from django.core.mail import send_mail
from django.views import View
import os

# Create your views here.

def index(request):
    # 人気順でソート
    ranks = Article.objects.order_by('-count')[:2] #降順
    objs = Article.objects.all()[:3]
    context = {
        'title' : 'Really Site',
        'articles' : objs,
        'ranks' : ranks,
    }
    return render(request, 'mysite/index.html', context)

class Login(LoginView):
    # ログイン/サインアップ用のサイトへ渡す
    template_name = 'mysite/auth.html'

    def form_valid(self,form):
        messages.success(self.request, 'ログイン完了！')
        # 親クラス(Loginview)のform_validを呼び出し。LoginViewで取得されたformが問題なければリダイレクトする オーバーライド
        return super().form_valid(form)

    def form_invalid(self,form):
        messages.error(self.request, 'ログイン失敗！')
        return super().form_invalid(form)

def signup(request):
    context = {}
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        # POSTで送られたユーザー情報をチェック、保存
        if form.is_valid():
            user = form.save(commit=False)
            #user.is_active = False
            user.save()
            # ログイン
            login(request, user)
            messages.success(request, '登録完了！')
            return redirect('/')
    # ログイン/サインアップ用のサイトへ渡す
    return render(request, 'mysite/auth.html', context)

# ログイン認証状態のみアクセス可能を指定
from django.contrib.auth.mixins import LoginRequiredMixin

class MypageView(LoginRequiredMixin,View):
    context = {}
    # クラスビューを使用。各メソッドでCRUDに対する処理を記載できる
    def get(self, request):
        return render(request, 'mysite/mypage.html', self.context)

    def post(self, request):
        #POSTリクエストでhtml側のフォームから送られたデータをdjangoのフォームモデルへ入れ込む nameで各fieldの値を指定
        form = ProfileForm(request.POST,request.FILES)
        if form.is_valid():
            profile = form.save(commit=False)
            profile.user = request.user
            profile.save()
            messages.success(request, '送信完了')
        else:
            messages.error(request, '更新失敗')
        return render(request, 'mysite/mypage.html',self.context)

class ContactView(View):
    context = {}
    def get(self, request):
        return render(request, 'mysite/contact.html', self.context)

    def post(self, request):
        # メール設定部分
        subject = "djangoテスト用問い合わせ"
        message = "お問い合わせ内容\n 名前: {} \n メールアドレス:{} \n 内容: {}".format(
            request.POST.get('name'),
            request.POST.get('email'),
            request.POST.get('content'))
        email_from = os.environ['DEFAULT_EMAIL_FROM']
        email_to = [
            os.environ['DEFAULT_EMAIL_FROM'],
        ]

        send_mail( subject, message, email_from, email_to, fail_silently=False,)
        messages.success(request,'お問い合わせ完了しました')

        return render(request, 'mysite/contact.html', self.context)

import payjp

class PayView(View):
    payjp.api_key = os.environ['PAYJP_SECRET_KEY']
    public_key = os.environ['PAYJP_PUBLIC_KEY']
    amount = 1000

    def get(self, request, *args, **kwargs):
        context = {
            'amount': self.amount,
            'public_key': self.public_key,
        }
        return render(request, 'mysite/pay.html', context=context)
    def post(self, request, *args, **kwargs):
        # テスト用の決済アカウントを使用
        customer = payjp.Customer.create(
            email = 'example@pay.jp',
            card = request.POST.get('payjp-token')
        )
        charge = payjp.Charge.create(
            amount = self.amount,
            currency = 'jpy',
            customer = customer.id,
            description ='支払いテスト')

        context = {
            'amount': self.amount,
            'public_key': self.public_key,
            'charge':charge
        }
        return render(request, 'mysite/pay.html', context=context)

def cache_test(request):
    import datetime
    context = {
        'time': datetime.datetime.now().isoformat(),
    }
    return render(request, 'mysite/cache_test.html', context)