from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

#ユーザー管理用マネージャー

class UserManager(BaseUserManager):
    #一般ユーザーの作成
    def create_user(self, email, password=None):
        #メアドチェック
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    #管理者用ユーザーの作成
    def create_superuser(self, email, password=None):
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

#ユーザーデータ
class User(AbstractBaseUser):
    email = models.EmailField(
        max_length=255,
        unique=True, #重複禁止
    )

    is_active = models.BooleanField(default=True)

    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

# --- OnetoOnefieldを同時作成、Userモデル作成時にProfileを作成・紐づけ ---
from django.db.models.signals import post_save
from django.dispatch import receiver

#@receiverデコレータでハンドラ呼び出し。post_saveでsenderで与えられたモデルが保存(作成)された後に直後にある関数を呼び出し
@receiver(post_save, sender=User)
def create_onetoone(sender, **kwargs):
    #receiverに渡された引数含む複数の引数をkwagsで取得。辞書として登録されるので、その中の「created」引数を確認
    if kwargs['created']: # post_save内にあるcreated引数を確認。レコードが新規作成分ならTrueとなる。
        from mysite.models.profile_models import Profile

        Profile.objects.create(user=kwargs['instance'])