from django.db import models
from django.contrib.auth import get_user_model
import os

def upload_image_to(instance, filename):
  user_id = str(instance.user.id)

  return os.path.join('image',user_id,filename)

class Profile(models.Model):
  # OneToOne使用  ユーザーモデルに紐づける + primary_key = Trueで主キーとする
  user = models.OneToOneField(get_user_model(),unique=True,on_delete=models.CASCADE,primary_key=True)

  username = models.CharField(default="John Doe",max_length=30)

  zipcode = models.CharField(default="",max_length=8) #郵便番号

  prefecture = models.CharField(default="",max_length=6) #都道府県

  city = models.CharField(default="",max_length=100) #都市

  address = models.CharField(default="",max_length=200) #地区番

  image = models.ImageField(default="", blank=True , upload_to=upload_image_to) #画像 URLパスが保存される