from django import forms
from django.db.models import fields
from blog.models import Comment

class CommentForm(forms.ModelForm):
  class Meta:
    model = Comment
    #formsで送信されるデータ 今回はコメントのみ
    fields = {
      'comment',
    }