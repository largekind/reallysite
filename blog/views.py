from django.shortcuts import render
from blog.models import Article , Comment , Tag
from django.core.paginator import Paginator
from blog.forms import CommentForm

# Create your views here.
def index(request):
    objs = Article.objects.all()
    pageginator = Paginator(objs,2) #一覧表示 n個ずつ
    page_number = request.GET.get('page') #GETリクエスト時、?pageというクエリキーをpage_numberに取得
    ''' NOTE
    HTTP GET page_number
    htttps://example.coom/kiji/?page=1 ...などのpage=の部分を取得
    ページ番号を今回は取得する
    '''

    context = {
      'page_title' : 'ブログ一覧',
      'page_obj' : pageginator.get_page(page_number),
      'page_number' : page_number,
    }
    return render(request,'blog/blogs.html', context)

def article(request, pk):
    # コメント投稿部分
    obj = Article.objects.get(pk=pk) #pathの引数を用いてArticleのID取得
    if request.method == 'POST':
      # いいねボタン
      if request.POST.get('like_count',None):
        obj.count += 1
        obj.save()
      # コメント投稿
      form = CommentForm(request.POST)
      if form.is_valid():
        comment = form.save(commit=False)
        comment.user = request.user
        comment.article = obj
        comment.save()

    comments = Comment.objects.filter(article=obj)
    #print(obj)
    context = {
      #'pk' : pk # primary key
      'article' : obj, #objのものをtemplateのarticleへ適用
      'comments' : comments
    }

    return render(request,'blog/article.html', context)

def tags(request,slug):
  tag = Tag.objects.get(slug=slug) #slugをもとにタグ情報取得
  # タグに関連する記事を取得
  '''
  アンダーバーで逆参照を行い、articleに関連するタグを取得する
  model名_setで中間テーブルを経由して逆参照が可能だが、本来はrelated_nameと呼ばれる引数が必要
  Tagは今回ManyToManyFieldで指定しているため、自動生成され上記のrelated_nameが無しでも逆参照が可能となっている
  '''
  objs = tag.article_set.all()
  pageginator = Paginator(objs,10) #一覧表示 n個ずつ
  page_number = request.GET.get('page') #GETリクエスト時、?pageというクエリキーをpage_numberに取得

  context = {
    'page_title' : '記事一覧#{}'.format(slug),
    'page_obj' : pageginator.get_page(page_number),
    'page_number' : page_number,
  }

  return render(request,'blog/blogs.html', context)