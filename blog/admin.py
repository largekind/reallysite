from django.contrib import admin
from blog.models import Article, Comment, Tag

# Register your models here.

class TagInLine(admin.TabularInline):
  model = Article.tags.through

class ArticleAdmin(admin.ModelAdmin):
  inlines = [TagInLine]
  exclude = ['tags',]

#registerの2つ目の引数に設定するクラスを指定することで、画面を編集可能
admin.site.register(Article,ArticleAdmin)
admin.site.register(Comment)
admin.site.register(Tag)
