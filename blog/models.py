from django.db import models
from django.db.models.base import Model
from django.contrib.auth import get_user_model
from django.db.models.fields.related import ManyToManyField

# Create your models here.
# データベース関連のモデル定義。各要素を示すスキームなどを定義する

class Tag(models.Model):
  slug = models.CharField(unique=True, primary_key=True, max_length=28)

  name = models.CharField(unique=True, max_length=28)

  def __str__(self):
      return self.slug


class Article(models.Model):  # テーブル名
  # 列titleを追加 デフォルト値は空文字(null挿入防止)
    title = models.CharField(default="", max_length=30)

    text = models.TextField(default="",)

    author = models.CharField(default="", max_length=30)

    created_at = models.DateField(auto_now_add=True)  # 作成日

    updated_at = models.DateField(auto_now=True)  # 更新日

    count = models.IntegerField(default=0) #いいね数

    tags = ManyToManyField(Tag,blank=True) #関連タグ


class Comment(models.Model):
  comment = models.TextField(default="", max_length=500)

  created_at = models.DateField(auto_now_add=True)  # 作成日 auto_now_addで作成した時の日時を保存

  '''
  外部キー使用。別のテーブルとつなぎ合わせる

  他にも繋ぎ合わせる方法として、下記が存在
  OneToOneメソッド : 1 対 1 (例 : ユーザー1人 対 プロフィール1つ)
  ForeignKeyメソッド : 1 対 多 (例 : ユーザー1人 対 コメント多数)
  ManyToManyメソッド : 多 対 多 (例 : 記事複数 対 タグ複数)
  '''
  user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
  '''
  get_user_model()にて下記setting.pyで使用されているユーザーモデルを読み込み、ユーザーモデルとして繋ぎ合わせてくれる
   > AUTH_USER_MODEL = 'mysite.User'
  on_deleteは外部キー参照されているオブジェクト削除時にどう対応するかの処理指定
  詳細 : https://mkai.hateblo.jp/entry/2018/08/11/101245
  '''

  article = models.ForeignKey(Article, on_delete=models.CASCADE)
