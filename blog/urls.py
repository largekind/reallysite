from django.urls import path, include
from blog import views
from blog.models import Article

# ブログ配下のURL一覧 (blog/**)
urlpatterns = [
    #path('test', views.test)
    path('', views.index),
    path('tags/<slug:slug>/', views.tags),  #blog/<<引数slug>>のURLで取得 view.py内のtagで与えられるので使用する
    path('<slug:pk>/', views.article),  #blog/<<引数pk>>のURLで取得 viewで使用する
]
