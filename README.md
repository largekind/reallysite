# ReallySite

django勉強用のリポジトリ。

# Requirement

* django 3.2
* livereload

# Installation

> pip install -r requirement.txt

# Usage

## 開発環境

以下のshを順に実行
> ./livereload.sh
> ./runserver.sh

## 静的ファイル生成

本番環境用の管理画面のCSS等を含んだ静的ファイルのデータ作成

> python manage.py collectstatic

## 本番環境

### デプロイ

デプロイ用のシェルを使用

> ./deploy.sh

中身は下記のコマンドの実行（静的ファイル生成 -> キャッシュテーブル生成 ->  デプロイ）

> python manage.py collectstatic
> python manage.py createcachetable
> gcloud app deploy --project elegant-gearing-321508

### Cluoud SQLへ接続

MySQLなのでTCPポートは3306としておく

> ./secrets/cloud_sql_proxy -instances="elegant-gearing-321508:us-central1:really-site-instance"=tcp:3306


ローカルのSqlLite3データーベースにある管理用画面へのログインはadmin@example.com/admin テスト用なので適当
GAEのデータベース(MySQL)側はパスワードマネージャー側の情報を参照して確認する
